ip route add default via 220.189.232.1
ip route add 220.189.234.64/26 via 220.189.234.246 metric 10
ip route add 220.189.234.64/26 via 220.189.234.253 metric 20
ip route add 220.189.234.160/27 via 220.189.234.246 metric 10
ip route add 220.189.234.160/27 via 220.189.234.253 metric 20
ip route add 220.189.234.128/27 via 220.189.234.242
ip route add 220.189.234.192/27 via 220.189.234.253 metric 10
ip route add 220.189.234.192/27 via 220.189.234.246 metric 20

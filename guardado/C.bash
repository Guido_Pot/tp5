ip route add default via 220.189.234.245
ip route add 220.189.234.0/26 via 220.189.234.245 metric 10
ip route add 220.189.234.0/26 via 220.189.234.249 metric 20
ip route add 220.189.234.128/27 via 220.189.234.245 metric 10
ip route add 220.189.234.128/27 via 220.189.234.249 metric 20
ip route add 220.189.234.160/27 via 220.189.234.238
ip route add 220.189.234.192/27 via 220.189.234.249 metric 10
ip route add 220.189.234.192/27 via 220.189.234.245 metric 20
ip route add 220.189.232.0/24 via 220.189.234.245 metric 10
ip route add 220.189.232.0/24 via 220.189.234.249 metric 20

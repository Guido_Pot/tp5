# TP5
Este repositorio contiene la resolucion del ejercicio pedido para la practica 5 de Sistemas Operativos y Redes [(E1224 / E0224)](https://www1.ing.unlp.edu.ar/catedras/E0224/)

El grupo 5 esta formado por los integrantes:

|Nombre|Numero de Alumno|
|------|----------------|
|Ireba Facundo|73611/4|
|Martinez Navamuel Manuel|71319/4|
|Potente Guido|73230/5|

## Ejecucion del programa

Hay dos formas disponibles de ejecutar el programa, usando `Final.imn` donde no es necesario usar ningun comando y simplemente con empezar la sesion se puede probar de hacer ping o traceroute, o el `finalConConfig.imn` que al ejecutar la sesion requiere abrir la terminal de algun router y ejecutar:

```Console
restore <Camino de la direccio>/tp5/gurdado
```

Esto permitira que pueda realizar los trace y pings necesarios.
> Es obligatorio tener instalado el save como lo menciona la guia de la catedra.
